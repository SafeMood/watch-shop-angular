export default class Article {
  id?: string;
  title?: string;
  image?: string;
  category?: string;
  excerpt?: string;
  body?: string;
  published?: boolean;
 }
