export default class Product {
    id?: string
    title?: string;
    image?: string;
    category?: string;
    price?: string;
}