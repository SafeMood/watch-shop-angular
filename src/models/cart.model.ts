export default class CartItem {
  id?: string;
  title?: string;
  image?: string;
  category?: string;
  price?: number;
  qte: number;
}
