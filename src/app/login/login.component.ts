import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../services/auth-service';
import { Router } from "@angular/router"
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = null
  errorMessage = ""
  successMessage = ""
  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  tryLogin(value) {
    this.authService.doLogin(value)
      .then(res => {
        console.log(res.user.uid)
        localStorage.setItem("user", JSON.stringify(res.user))
        this.authService.checkAdmin()
        this.errorMessage = "";
        this.successMessage = "logged In ";
        this.router.navigate(['/home'])

      }, err => {
        console.log(err);
        this.errorMessage = err.message;
        this.successMessage = "";
      })
  }

}
