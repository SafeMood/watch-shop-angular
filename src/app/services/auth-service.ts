import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<any>;
  constructor(private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router) {


  }


  doRegister(value) {

    return new Promise<any>((resolve, reject) => {

      this.afAuth.createUserWithEmailAndPassword(value.email, value.password)
        .then(res => {
          resolve(res);
        }, err => reject(err))
    })
  }

  doLogin(value) {
    return this.afAuth.signInWithEmailAndPassword(value.email, value.password)
  }

  checkAdmin() {
    var user = this.getCurrentUser();
    if (user) {
      return this.afs.collection('admins').ref.where("id", "==", user.uid).onSnapshot((res) => {
        let isAdmin: string = res.size > 0 ? "1" : "0";
        return localStorage.setItem("isAdmin", isAdmin)
      })
    } else {
      return false
    }
  }
  isAdmin() {
    return !!Number(localStorage.getItem("isAdmin"))
  }
  getCurrentUser() {
    return JSON.parse(localStorage.getItem("user"));
  }
  isLoggedIn() {
    return !!localStorage.getItem("user");
  }
  doLogout() {
    localStorage.clear();
    return this.afAuth.signOut()
  }


}
