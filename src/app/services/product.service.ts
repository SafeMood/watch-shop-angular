import { Injectable, Query } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, QuerySnapshot } from '@angular/fire/firestore';
import Product from 'src/models/product.model';


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private dbPath = '/products';

  productsRef: AngularFirestoreCollection<Product> = null;

  constructor(private db: AngularFirestore) {
    this.productsRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<Product> {
    return this.productsRef;
  }

  getAllOrderByPrice() {
    return this.productsRef.ref.orderBy("price", "desc");
  }

  create(product: Product): any {
    return this.productsRef.add({ ...product });
  }

  update(id: string, data: any): Promise<void> {
    return this.productsRef.doc(id).update(data);
  }

  delete(id: string): Promise<void> {
    return this.productsRef.doc(id).delete();
  }

}
