import { Component, OnInit } from '@angular/core';
import Article from 'src/models/article.model';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-blog-admin',
  templateUrl: './blog-admin.component.html',
  styleUrls: ['./blog-admin.component.scss'],
})
export class BlogAdminComponent implements OnInit {
  articles: Article[];
  editItem: Article;
  addItem: Article;
  categories: {};
  hideEditForm: boolean = true;
  hideAddForm: boolean = true;
  constructor(private blogService: BlogService) {}

  ngOnInit(): void {
    this.getAllArticles();
    this.categories = ['new', 'popular'];
  }
  getAllArticles() {
    this.blogService
      .getAll()
      .snapshotChanges()
      .subscribe((res) => {
        this.articles = res.map((e) => {
          return {
            id: e.payload.doc.id,
            ...e.payload.doc.data(),
          } as Article;
        });
      });
  }

  showEditForm(el) {
    this.hideEditForm = !this.hideEditForm;
    this.hideAddForm = true;
    this.editItem = el;
  }
  // fix cancel in edit (done)
  // fix update (done)
  // fix admin check (done)
  // fix save (done)
  // fix panel notification (done)
  onChange(event: any) {
    this.addItem = {
      ...this.addItem,
      [event.target.name]: event.target.value,
    };
  }
  addArticle(item) {
    console.log(item)
    this.blogService.create(item);
    this.hideForm();
  }

  updateArticle() {
    this.blogService.update(this.editItem.id, this.editItem);
    this.hideForm();
  }
  showAddForm() {
    this.hideAddForm = !this.hideAddForm;
    this.hideEditForm = true;
  }
  hideForm() {
    this.hideAddForm = true;
    this.hideEditForm = true;
  }
  DeleteItem(el) {
    this.blogService.delete(el.id);
  }
}
