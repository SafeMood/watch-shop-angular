import { Component, OnInit } from '@angular/core';
import Article from 'src/models/article.model';
import { BlogService } from '../blog.service';
import { AuthService } from '../services/auth-service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
})
export class BlogComponent implements OnInit {
  articles: Article[];
  article: Article;
  submitted = false;
  constructor(
    private authService: AuthService,
    private blogService: BlogService
  ) {}

  ngOnInit(): void {
    this.getArticles();
  }

  getArticles() {
    this.blogService
      .getAll()
      .snapshotChanges()
      .subscribe((res) => {
        this.articles = res.map((e) => {
          return {
            id: e.payload.doc.id,
            ...e.payload.doc.data(),
          } as Article;
        });
      });
  }

  saveArticle(): void {
    this.blogService.create(this.article).then(() => {
      console.log('Created new item successfully!');
      this.submitted = true;
    });
  }

  newArticle(): void {
    this.submitted = false;
    this.article = new Article();
  }
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }
}
