import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import firebase from 'firebase';
import Article from 'src/models/article.model';
import { AuthService } from './services/auth-service';

@Injectable({
  providedIn: 'root',
})
export class BlogService {
  private dbPath = '/articles';

  articlesRef: AngularFirestoreCollection<Article> = null;

  constructor(private authService: AuthService, private db: AngularFirestore) {
    this.articlesRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<Article> {
    return this.articlesRef;
  }

  getArticleById(id) {
    return this.articlesRef.ref.doc(id);
  }
  create(data) {
    const createdAt = firebase.firestore.FieldValue.serverTimestamp();
    return this.articlesRef.add({
      ...data,
      createdAt,
    });
  }

  update(id: string, data: any): Promise<void> {
    const updatedAt = firebase.firestore.FieldValue.serverTimestamp();

    return this.articlesRef.doc(id).update({
      ...data,
      updatedAt,
    });
  }

  delete(id: string): Promise<void> {
    return this.articlesRef.doc(id).delete();
  }
}
