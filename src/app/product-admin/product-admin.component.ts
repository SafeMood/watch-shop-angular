import { Component, OnInit } from '@angular/core';
import Product from 'src/models/product.model';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-admin',
  templateUrl: './product-admin.component.html',
  styleUrls: ['./product-admin.component.scss']
})
export class ProductAdminComponent implements OnInit {

  products: Product[];
  editItem: Product;
  addItem: Product;
  categories: {};
  hideEditForm: boolean = true;
  hideAddForm: boolean = true;
  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.getAllProducts();
    this.categories = ['new', 'popular'];
  }
  getAllProducts() {
    this.productService
      .getAll()
      .snapshotChanges()
      .subscribe((res) => {
        this.products = res.map((e) => {
          return {
            id: e.payload.doc.id,
            ...e.payload.doc.data(),
          } as Product;
        });
      });
  }

  showEditForm(el) {
    this.hideEditForm = !this.hideEditForm;
    this.hideAddForm = true;
    this.editItem = el;
  }
  // fix cancel in edit (done)
  // fix update (done)
  // fix admin check (done)
  // fix save (done)
  // fix panel notification (done)
  onChange(event: any) {
    this.addItem = {
      ...this.addItem,
      [event.target.name]: event.target.value,
    };
  }
  addProduct(item) {
    this.productService.create(item);
    this.hideForm();
  }

  updateProduct() {
    this.productService.update(this.editItem.id, this.editItem);
    this.hideForm();
  }
  showAddForm() {
    this.hideAddForm = !this.hideAddForm;
    this.hideEditForm = true;
  }
  hideForm() {
    this.hideAddForm = true;
    this.hideEditForm = true;
  }
  DeleteItem(el) {
    this.productService.delete(el.id);
  }

}
