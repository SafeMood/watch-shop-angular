import { Component, OnInit } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.scss'],
})
export class BlogDetailsComponent implements OnInit {
  id;
  article: any;
  constructor(
    private afs: AngularFirestore,
    private route: ActivatedRoute,
    private blogService: BlogService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.article = this.getArticleById(this.id);
  }

  getArticleById(id) {
    this.blogService.getArticleById(id).onSnapshot((querySnapshot) => {
      console.log(querySnapshot);
      this.article = {
        id: querySnapshot.id,
        ...querySnapshot.data(),
      };
    });
  }
}
