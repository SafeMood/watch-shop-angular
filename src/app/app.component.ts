import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import Product from 'src/models/product.model';
import { AuthService } from './services/auth-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'watchshopv2';
  admins = []
  newProducts = new Array(10).fill({
    title: "Gen 5E Darci Pavé Gold-Tone",
    image: "https://michaelkors.scene7.com/is/image/MichaelKors/MKT5127-0710_1?wid=361&hei=380&op_sharpen=1&resMode=sharp2&qlt=90",
    category: "popular",
    price: "600"
  });
   constructor(private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router) {
  }


  ngOnInit(): void { 
  }


}
