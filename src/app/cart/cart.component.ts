import { Component, OnInit } from '@angular/core';
import CartItem from 'src/models/cart.model';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  items: any;

  constructor(private cartService: CartService) {}

  ngOnInit(): void {
    this.getCartItems();
  }

  getCartItems() {
    this.cartService.getCurrentUserItems().onSnapshot((querySnapshot) => {
      this.items = querySnapshot.docChanges().map((e) => {
        return ({
          id: e.doc.id,
          ...e.doc.data(),
        } as unknown) as CartItem;
      });
    });
  }
  addItemQte(item) {
    item.qte += 1;
    this.cartService.update(item.id, item);
  }
  subItemQte(item) {
    if (Number(item.qte) == 0) {
      return;
    }
    item.qte -= 1;
    this.cartService.update(item.id, item);
  }
  removeItemFromCart(item) {
    this.cartService.delete(item.id).then(() => {
      this.items = this.items.filter((v) => item.id != v.id);
      console.log('Deleted item successfully!');
    });
  }
  getTotal() {
    return this.items
      .map((item) => {
        return Number(item.qte) * Number(item.price);
      })
      .reduce((t, item) => {
        return t + item;
      });
  }
}
