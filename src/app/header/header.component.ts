import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { AuthService } from '../services/auth-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  numberOfItems: number = 0;
  constructor(private cartService: CartService, private authService: AuthService) {

  }

  ngOnInit(): void {
    this.cartService.getCurrentUserItems().onSnapshot((querySnapshot) => {
      this.numberOfItems = querySnapshot.size
    })
  }
  isLoggedIn() {
    return this.authService.isLoggedIn()
  }
  isAdmin() {
    return this.authService.isAdmin()
  }
  logOut() {
    return this.authService.doLogout()
  }
}
