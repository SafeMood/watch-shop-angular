import { Component, OnInit } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Observable, OperatorFunction } from 'rxjs';
import { mergeMap, switchMap } from 'rxjs/operators';
import Product from 'src/models/product.model';
import { ProductService } from '../services/product.service';
import { AuthService } from '../services/auth-service';
import { CartService } from '../cart.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
  product: Product = new Product();
  productsCollection: AngularFirestoreCollection<Product>;
  products: Product[];
  productsOrderByPrice: Product[];
  submitted = false;
  constructor(
    private afAuth: AngularFireAuth,
    private cartService: CartService,
    private authService: AuthService,
    private productService: ProductService,
    private afs: AngularFirestore
  ) {}

  ngOnInit() {
    this.getAllProducts();
    this.getProductsOrderByPrice();
    console.log(this.productsOrderByPrice);
  }

  getAllProducts() {
    this.productService
      .getAll()
      .snapshotChanges()
      .subscribe((res) => {
        this.products = res.map((e) => {
          return {
            id: e.payload.doc.id,
            ...e.payload.doc.data(),
          } as Product;
        });
      });
  }
  getProductsOrderByPrice() {
    this.productService.getAllOrderByPrice().onSnapshot((res) => {
      this.productsOrderByPrice = res.docChanges().map((e) => {
        return {
          id: e.doc.id,
          ...e.doc.data(),
        } as Product;
      });
    });
  }
  saveProduct(): void {
    this.productService.create(this.product).then(() => {
      console.log('Created new item successfully!');
      this.submitted = true;
    });
  }

  newProduct(): void {
    this.submitted = false;
    this.product = new Product();
  }
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }
  addToCart(item): void {
    let user = this.authService.getCurrentUser();
    this.cartService.create({ ...item, qte: 1, userId: user.uid }).then(() => {
      this.cartService.getCurrentUserItems();
      console.log('added new item successfully!');
    });
  }

  deleteFromCart(item): void {
    this.cartService.delete(item.id).then(() => {
      this.cartService.getCurrentUserItems();
      console.log('Deleted item successfully!');
    });
  }
}
