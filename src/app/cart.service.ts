import { Injectable, Query } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import Product from 'src/models/product.model';
import { AuthService } from './services/auth-service';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private dbPath = '/cart';

  cartRef: AngularFirestoreCollection<Product> = null;

  constructor(private authService: AuthService, private db: AngularFirestore) {
    this.cartRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<Product> {
    return this.cartRef;
  }

  getCurrentUserItems() {
    let user = this.authService.getCurrentUser();

    return this.cartRef.ref.where('userId', '==', user.uid);
  }

  create(item) {
    return this.cartRef.doc(item.id).set(item);
  }

  update(id: string, data: any): Promise<void> {
    return this.cartRef.doc(id).update(data);
  }

  delete(id: string): Promise<void> {
    return this.cartRef.doc(id).delete();
  }
}
