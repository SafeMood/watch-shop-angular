import { Component, OnInit, TestabilityRegistry } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import Product from 'src/models/product.model';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {
  productsTab: boolean = true;
  blogTab: boolean = false;

  onClickProductsTab() {
    this.productsTab = true;
    this.blogTab = false;
  }
  onClickArticlesTab() {
    this.productsTab = false;
    this.blogTab = true;
  }
}
