# Watch shop :zap:

E-commerce Shop with angular and firestore

- login && registration

- Admin Section

  -Products Crud

  -Articles Crud

- User Section

  - Home
  - About
  - Shop
  - Cart Shop
  - Blog
  - Blog Details
  - Contact
  - Login
  - Registration

## install dependencies

```bash
    npm install
```

## Run Project

```bash
    ng serve
```

[You can see it online from here -->](https://watchshop.herokuapp.com/watch/home/)
